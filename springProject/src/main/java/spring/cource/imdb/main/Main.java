package spring.cource.imdb.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.cource.imdb.dao.ActorsDAO;
import spring.cource.imdb.dao.daos.ActorsDaoImpl;
import spring.cource.imdb.entities.Actor;

import java.util.Collection;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
//        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("app-context.xml");
        ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext("spring.cource.imdb.service", "spring.cource.imdb.dao.daos", "spring.cource.imdb.conf");
        ctx.registerShutdownHook();
        Collection<Actor> actors = ctx.getBean("actorsDaoImpl", ActorsDAO.class).getActors().values();
        actors.forEach(logger::info);
    }
}
