package spring.cource.imdb.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import spring.cource.imdb.dao.ActorsDAO;
import spring.cource.imdb.dao.MovieDAO;
import spring.cource.imdb.dao.daos.ActorsDaoImpl;
import spring.cource.imdb.dao.daos.MoviesDaoImpl;
import spring.cource.imdb.entities.Actor;
import spring.cource.imdb.entities.Movie;
import spring.cource.imdb.events.RatingSetEvent;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@ComponentScan("spring.cource.imdb.dao.daos")
public class IMDBServiceImpl implements IMDBService {
    private static Logger logger = LogManager.getLogger(IMDBServiceImpl.class);

    @Autowired
    private MoviesDaoImpl movieDAO;
    @Autowired
    private ActorsDaoImpl actorsDAO;

    @Override
    public int totalDefinedMovies() {
        return movieDAO.getMovies().size();
    }

    @Override
    public int totalDefinedActors() {
        return actorsDAO.getActors().size();
    }

    @Override
    public List<Actor> getActorsOfMovie(int movieID) {
        return movieDAO
                .getMovies()
                .entrySet()
                .stream()
                .filter(entry->entry.getKey() == movieID)
                .map(Map.Entry::getValue)
                .map(Movie::getActors)
                .findFirst()
                .orElse(Collections.EMPTY_LIST);

    }

    @Override
    public List<Movie> getAllMoviesForActor(int actorID) {
        return movieDAO
                .getMovies()
                .values()
                .stream()
                .filter(movie -> movie.getActors().stream().anyMatch(actor -> actor.getId() == actorID))
                .collect(Collectors.toList());
    }

    @EventListener
    public void updateEgo(RatingSetEvent event) {
        Movie movie = event.getMovie();
        getActorsOfMovie(movie.getId()).forEach(actor -> actor.getEgo().addMovie(movie));
    }

    @PostConstruct
    @Override
    public void init() {
        actorsDAO
                .getActors()
                .forEach((actorId, actor) ->
                getAllMoviesForActor(actorId)
                        .forEach(actor.getEgo()::addMovie));
    }

    @PreDestroy
    public void destroy(){
        logger.info("Destroyed");
    }
}
