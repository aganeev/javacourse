package spring.cource.imdb.service;

import spring.cource.imdb.entities.Actor;
import spring.cource.imdb.entities.Movie;

import java.util.List;

public interface IMDBService {
    int totalDefinedMovies();
    int totalDefinedActors();
    List<Actor> getActorsOfMovie(int movieID);
    List<Movie> getAllMoviesForActor(int actorID);
    void init();


}
