package spring.cource.imdb.service;

import spring.cource.imdb.dao.ActorsDAO;
import spring.cource.imdb.dao.MovieDAO;

public class IMDBFactory {
    private static final IMDBFactory instance;

    static {
        instance = new IMDBFactory();
    }

    private IMDBFactory() {
    }

    public static IMDBFactory getInstance(){
        return instance;
    }

    public IMDBService createIMDBService(){
        return new IMDBServiceImpl();
    }
}
