package spring.cource.imdb.entities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.lang.Nullable;

import java.util.HashSet;
import java.util.Set;

public class EntitiesMapBeansPostProcessor implements BeanPostProcessor {

    private static Logger logger = LogManager.getLogger(EntitiesMapBeansPostProcessor.class);
    private Set<Integer> knownActors;
    private Set<Integer> knownMovies;

    public EntitiesMapBeansPostProcessor() {
        knownActors = new HashSet<>();
        knownMovies = new HashSet<>();
    }

    @Nullable
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {

        if (bean instanceof Actor) {
            handleEntity(((Actor)bean).getId(), knownActors, "Actor");
        } else if (bean instanceof Movie) {
            handleEntity(((Movie)bean).getId(), knownMovies, "Movie");
        }

        return bean;
    }

    private void handleEntity(int id, Set<Integer> set, String setEntityName) {
        logger.info("Bean post processor: Processing {} ID {}..", setEntityName, id);
        if (set.contains(id)) {
            logger.error("Bean post processor: {} ID {} already exists !", setEntityName, id);

        } else {
            set.add(id);
            logger.info("Bean post processor: Accepting {} ID {}.. so far encountered {} {}s", setEntityName, id, set.size(), setEntityName);
        }
    }
}
