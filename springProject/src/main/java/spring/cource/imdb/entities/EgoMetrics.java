package spring.cource.imdb.entities;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class EgoMetrics {
    private int totalMovies;
    private double averageMovieRating;

    public EgoMetrics() {
        totalMovies = 0;
        averageMovieRating = 0;
    }

    public void addMovie(Movie movie) {
        double totalScore = averageMovieRating * totalMovies++;
        totalScore += movie.getRating();
        averageMovieRating = totalScore / totalMovies;
    }

    public int getTotalMovies() {
        return totalMovies;
    }

    public double getAverageMovieRating() {
        return averageMovieRating;
    }

    @Override
    public String toString() {
        return "EgoMetrics{" +
                "totalMovies=" + totalMovies +
                ", averageMovieRating=" + averageMovieRating +
                '}';
    }
}
