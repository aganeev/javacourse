package spring.cource.imdb.entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import spring.cource.imdb.enums.Gender;
import spring.cource.imdb.events.RatingSetEvent;

import java.util.Objects;

@ComponentScan("spring.cource.imdb.entities")
public class Actor {
    private int id;
    private String name;
    private int age;
    private Gender gender;
    @Autowired
    private EgoMetrics ego;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setEgo(EgoMetrics ego) {
        this.ego = ego;
    }

    public EgoMetrics getEgo() {
        return ego;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", ego=" + ego +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Actor)) return false;
        Actor actor = (Actor) o;
        return id == actor.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    public int getId() {
        return id;
    }

}
