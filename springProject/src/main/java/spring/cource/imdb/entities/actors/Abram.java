package spring.cource.imdb.entities.actors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import spring.cource.imdb.entities.Actor;
import spring.cource.imdb.enums.Gender;

import javax.annotation.PostConstruct;


@Component
public class Abram extends Actor {

    @Value("${abram.id}")
    private int id;

    @Value("${abram.name}")
    private String name;

    @Value("${abram.age}")
    private int age;

    @Value("${abram.gender}")
    private Gender gender;

    public Abram(){
        super();
    }

    @PostConstruct
    public void init(){
        this.setId(id);
        this.setName(name);
        this.setAge(age);
        this.setGender(gender);
    }

}
