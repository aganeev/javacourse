package spring.cource.imdb.entities.moovies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.cource.imdb.entities.Movie;
import spring.cource.imdb.entities.actors.Bill;
import spring.cource.imdb.entities.actors.Shelly;
import spring.cource.imdb.enums.Genre;

import javax.annotation.PostConstruct;

@Component
public class Moooovie2 extends Movie {
    @Autowired
    private Shelly shelly;

    @Autowired
    private Bill bill;


    public Moooovie2(){
        super(2,"Movie2",Genre.DRAMA,5);
    }

    @PostConstruct
    void fillActors(){
        getActors().add(shelly);
        getActors().add(bill);
    }
}
