package spring.cource.imdb.entities.actors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import spring.cource.imdb.entities.Actor;
import spring.cource.imdb.enums.Gender;

import javax.annotation.PostConstruct;

@Component
public class Shelly extends Actor {

    @Value("${shelly.id}")
    private int id;

    @Value("${shelly.name}")
    private String name;

    @Value("${shelly.age}")
    private int age;

    @Value("${shelly.gender}")
    private Gender gender;

    public Shelly(){
        super();
    }

    @PostConstruct
    public void init(){
        this.setId(id);
        this.setName(name);
        this.setAge(age);
        this.setGender(gender);
    }

}
