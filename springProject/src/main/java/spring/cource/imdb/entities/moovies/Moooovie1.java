package spring.cource.imdb.entities.moovies;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import spring.cource.imdb.entities.Movie;
import spring.cource.imdb.entities.actors.Abram;
import spring.cource.imdb.entities.actors.Bill;
import spring.cource.imdb.enums.Genre;

import javax.annotation.PostConstruct;

@Component
@ComponentScan("spring.cource.imdb.entities.actors")
public class Moooovie1 extends Movie {

    @Autowired
    private Abram abram;

    @Autowired
    private Bill bill;


    public Moooovie1(){
        super(1,"Mooovie1",Genre.ACTION,8);
    }

    @PostConstruct
    void fillActors(){
        getActors().add(abram);
        getActors().add(bill);
    }
}
