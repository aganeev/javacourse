package spring.cource.imdb.entities.actors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import spring.cource.imdb.entities.Actor;
import spring.cource.imdb.enums.Gender;

import javax.annotation.PostConstruct;

@Component
public class Bill extends Actor {

    @Value("${bill.id}")
    private int id;

    @Value("${bill.name}")
    private String name;

    @Value("${bill.age}")
    private int age;

    @Value("${bill.gender}")
    private Gender gender;

    public Bill(){
        super();
    }

    @PostConstruct
    public void init(){
        this.setId(id);
        this.setName(name);
        this.setAge(age);
        this.setGender(gender);
    }
}
