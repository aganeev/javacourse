package spring.cource.imdb.entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import spring.cource.imdb.enums.Genre;
import spring.cource.imdb.events.RatingSetEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Movie {
    private int id;
    private String name;
    private Genre genre;
    private List<Actor> actors;
    private int rating;

    @Autowired
    private ApplicationContext applicationContext;

    public Movie(int id, String name, Genre genre, int rating) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.rating = rating;
        actors = new ArrayList<>();
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public int getId() {
        return id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
        applicationContext.publishEvent(new RatingSetEvent(this));
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre=" + genre +
                ", actors=" + actors +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;
        Movie movie = (Movie) o;
        return id == movie.id;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
