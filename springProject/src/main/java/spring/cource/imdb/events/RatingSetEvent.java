package spring.cource.imdb.events;

import org.springframework.context.ApplicationEvent;
import spring.cource.imdb.entities.Movie;

public class RatingSetEvent extends ApplicationEvent {
    private Movie movie;
    public RatingSetEvent(Object source) {
        super(source);
        this.movie = (Movie) source;
    }

    public Movie getMovie() {
        return movie;
    }
}
