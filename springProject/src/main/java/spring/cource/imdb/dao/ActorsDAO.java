package spring.cource.imdb.dao;

import spring.cource.imdb.entities.Actor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ActorsDAO {
    private Map<Integer,Actor> actors;

    @Override
    public String toString() {
        return "ActorsDAO{" +
                "actors=" + actors +
                '}';
    }

    public Map<Integer, Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors.stream()
                .collect(Collectors.toMap(Actor::getId, actor->actor));
    }
}
