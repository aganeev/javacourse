package spring.cource.imdb.dao;

import spring.cource.imdb.entities.Movie;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MovieDAO {
    private Map<Integer,Movie> movies;

    public void setMovies(List<Movie> movies) {
        this.movies = movies.stream()
            .collect(Collectors.toMap(Movie::getId, movie->movie));
    }

    @Override
    public String toString() {
        return "MovieDAO{" +
                "movies=" + movies +
                '}';
    }

    public Map<Integer, Movie> getMovies() {
        return movies;
    }
}
