package spring.cource.imdb.dao.daos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import spring.cource.imdb.dao.MovieDAO;
import spring.cource.imdb.entities.Movie;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@ComponentScan("spring.cource.imdb.entities.moovies")
public class MoviesDaoImpl extends MovieDAO {
    @Autowired
    private List<Movie> moviesList;

    public MoviesDaoImpl(){
        super();
    }

    @PostConstruct
    void fillMovies(){
        this.setMovies(moviesList);
    }

}
