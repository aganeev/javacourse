package spring.cource.imdb.dao.daos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import spring.cource.imdb.dao.ActorsDAO;
import spring.cource.imdb.entities.Actor;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@ComponentScan("spring.cource.imdb.entities.actors")
public class ActorsDaoImpl extends ActorsDAO {
    @Autowired
    private List<Actor> actorsList;

    public ActorsDaoImpl(){
        super();
    }

    @PostConstruct
    void fillActors(){
        this.setActors(actorsList);
    }

}
